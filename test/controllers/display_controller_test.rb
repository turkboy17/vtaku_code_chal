require "test_helper"

class DisplayControllerTest < ActionDispatch::IntegrationTest
  test "should get images" do
    get display_images_url
    assert_response :success
  end
end
