# README

Start up:
	Only a web browser and internet conection is required to access this application. 
	Heroku url: https://vtakucodechal.herokuapp.com/

Use:
	When connecting to the application the user enters a title and submits an image to be uploaded. 
	The image and title is entered into the database.
	The user then can navigate to the "Display Page" and view the submitted images. 



Ruby version
	2.7.4p191 (2021-07-07 revision a21a3b7d23) [x64-mingw32]
Rails version
	6.1.4
Heroku version
	7.53.0 win32-x64 node-v12.21.0
PostgreSQL version
	13.3
Git version
	2.31.0.windows.1

