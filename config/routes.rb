Rails.application.routes.draw do
  resources :images
  #get 'display/images'
  get 'images/index'
  root 'images#new'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
